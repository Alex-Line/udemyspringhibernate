package com.iteco.linealex.udemyspringhibernate.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Student {

    private String firstName;

    private String secondName;

}