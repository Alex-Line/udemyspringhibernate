<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
    <head>
        Hello World Page
    </head>
    <body>
        <h1>Hello World of Spring</h1>
        <br><br>
        Student name: ${param.studentName}
        <br><br>
        The message: ${message}
    </body>
</html>