<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!Document html>
<html>
    <head>
        <title>Student Registration Form</title>
    </head>
    <body>
        <form:form action="processForm" modelAttribute="student">
            First name: <form:input path="firstName"/>
            <br><br>
            Second name: <form:input path="secondName" />
            <br><br>
            <input type="submit" value="Submit" />
        </form:form>
    </body>
</html>